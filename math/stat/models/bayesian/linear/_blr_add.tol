//////////////////////////////////////////////////////////////////////////////
// FILE   : blr_add.tol
// PURPOSE: 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// INCLUDES
//////////////////////////////////////////////////////////////////////////////
//Set Include(PathIniSadd(0));

//////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// PROCEDURES
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Set BLR_AddPrior(Set model)
//////////////////////////////////////////////////////////////////////////////
{
  Set sY          = model->SY;
  Set sX          = model->SX;
  Set inputDef    = model->InputDefinition;
  Set sigmaDef    = model->SigmaDefinition;

  Set sYP         = DeepCopy(sY);
  Set sXP         = DeepCopy(sX);
  Set sigmaDefP   = DeepCopy(sigmaDef);
  Set infoRowSX   = sX[Card(sX)];  
  Real lastRowSX  = infoRowSX[1]+Rows(infoRowSX[3])-1;

  Real   k = 1;
  Real While(LE(k, Card(inputDef)),
  {
    Set reg = inputDef[k];
    Set priorInfo  = reg->PriorInfo;
     
    If(IsEmpty(priorInfo), False,
    {
      Text class = ToUpper(priorInfo->ClassName);
      Case
      (
        class == "NORMAL", 
        {
          Real lastRowSX := lastRowSX + 1;
          Real row = Copy(lastRowSX);

// Incluir comprobaciones de los datos, probablemente esto debe 
// ir en un modulo superior a este e independiente que chequee una estructura
// BLRModelDef

          Set param   = priorInfo->Parameters;
          Real nu     = param->Nu;
          Real sigma  = param->Sigma;

          Set sYBlock = SetOfAnything(Diag(1, nu));
          Set sXBlock = SetOfAnything(row, Copy(k), Diag(1, 1));
  
          Text namePar     = reg->Name; 
          Text nameSig     = "SigmaPrior."+namePar; 
          Text class       = "FIXED";
          Set parameters   = BLRPriorInfo.Fixed(sigma); 
          Set priorSig     = BLRPriorInfo(class, parameters);
          Set sigmaDefPReg = BLRParamDef
          (
            nameSig, 
            sigma,  
            priorSig,  
            Copy(Empty)                 
          );

          Set sYP := sYP<<sYBlock;
          Set sXP := sXP<<SetOfSet(sXBlock);
          Set sigmaDefP := sigmaDefP<<SetOfSet(sigmaDefPReg);
          True         
        },
        1, False
      )  
    });  
    Real k := k+1;    
    k 
  }); 
  
  BLRModelDef(sYP, sXP, Empty, inputDef, sigmaDefP)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Introduce la informacion a priori de los parametros como un 
caso particular de regresion lineal ampliada.", 
BLR_AddPrior);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set BLR_GetRoots(Set nodeInfo)
//////////////////////////////////////////////////////////////////////////////
{
  Set selRoot = EvalSet(nodeInfo, Text(Set node)
  {
    node->Root
  });
  Set roots = Unique(selRoot);
  roots
};

//////////////////////////////////////////////////////////////////////////////
Set BLR_GetSheets(Text root, Set nodeInfo)
//////////////////////////////////////////////////////////////////////////////
{
  Set selRoot = Select(nodeInfo, Real(Set node)
  {
    node->Root == root
  });
  Set sheets = If(IsEmpty(selRoot), Empty, 
  EvalSet(selRoot, Set(Set node)
  { 
    Text nameSheet  = node->Sheet;
    Real scaleSheet = node->Scale;
    Set reg = SetOfAnything(nameSheet, scaleSheet);
    Eval("Set "+nameSheet+" = reg")
  }));
  sheets
};

//////////////////////////////////////////////////////////////////////////////
Matrix BLR_GetHiperBlock(Set allParamNames, Set sheets, Text hpName)
//////////////////////////////////////////////////////////////////////////////
{
  Real numAllPar  = Card(allParamNames);
  Set nSheets     = Traspose(sheets)[1]; 

  Set index       = Range(1, numAllPar, 1);
  Set indexSheets = Select(index, Real(Real i){allParamNames[i] <: nSheets});
  Set indexHp     = Select(index, Real(Real i){allParamNames[i] == hpName});

  Matrix one    = Rand(1,1,1,1);
  Matrix oneCol = Rand(Card(sheets),1,-1,-1);
  Matrix matrixSheet = Group("ConcatRows", EvalSet(indexSheets, Matrix(Real i)
  {
    Text nIndex = allParamNames[i];
    Real scale  = sheets[nIndex][2]; 
    FullWithZeroCol(RProd(one, 1/scale), i, numAllPar) 
  }));
  Matrix matrixHp = FullWithZeroCol(oneCol, indexHp[1], numAllPar);
  matrixSheet+matrixHp
};

//////////////////////////////////////////////////////////////////////////////
Set BLR_AddHierarchy(Set model, Set hierSet)
//////////////////////////////////////////////////////////////////////////////
{
  Set sY          = model->SY;
  Set sX          = model->SX;
  Set inputDef    = model->InputDefinition;
  Set sigmaDef    = model->SigmaDefinition;

  Set hierParam       = hierSet->ParamSet;
  Set hierTree        = hierSet->Tree;
  Set hierSigmaBlock  = hierSet->SigmaBlock;  

  Set sYH         = DeepCopy(sY);
  Set sXH         = DeepCopy(sX);
  Set inputDefH   = inputDef<<hierParam;
  Set sigmaDefH   = DeepCopy(sigmaDef);

  Set allParamNames  = Traspose(inputDefH)[1];

  Set infoRowSX   = sX[Card(sX)];  
  Real lastRowSX  = infoRowSX[1]+Rows(infoRowSX[3])-1;

  Real   k = 1;
  Real While(LE(k, Card(hierParam)),
  {
    Set reg       = hierParam[k];
    Text hpName   = reg->Name;

    Set hpSheets  = BLR_GetSheets(hpName, hierTree);

    Real lastRowSX := lastRowSX + 1;
    Real row = Copy(lastRowSX);
    
    Matrix mat = BLR_GetHiperBlock(allParamNames, hpSheets, hpName);

    Set sYBlock = SetOfMatrix(Rand(Card(hpSheets), 1, 0, 0));
    Set sXBlock = SetOfAnything(row, 1, mat);

    Text nameSig     = "SigmaHier."+hpName; 
    Text class       = "FIXED";
    Set selSigma = Select(hierSigmaBlock, Real(Set reg)
    {
      Text name = reg->Name;
      name == hpName
    });
    Real sigma       = If(IsEmpty(selSigma), ?, selSigma[1]->Sigma);
    Set parameters   = BLRPriorInfo.Fixed(sigma); 
    Set priorSig     = BLRPriorInfo(class, parameters);
    Set sigmaDefHReg = BLRParamDef
    (
      nameSig, 
      0.1,  
      priorSig,  
      Copy(Empty)                 
    );

    Set sYH := sYH<<sYBlock;
    Set sXH := sXH<<SetOfSet(sXBlock);
    Set sigmaDefH := sigmaDefH<<SetOfSet(sigmaDefHReg);
    
    Real lastRowSX := lastRowSX+Rows(mat);
    Real k := k+1;    
    k 
  }); 
  
  BLRModelDef(sYH, sXH, Empty, inputDefH, sigmaDefH)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("", 
BLR_AddHierarchy);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set BLR_UnionModelDef(Set model1, Set model2)
//////////////////////////////////////////////////////////////////////////////
{
  Set sY = (model1->SY) << (model2->SY);
  
  Set sXUnion = ((model1->SX) << (model2->SX))- SetOfText("TRIPLET");
  Set sX = SetOfText("TRIPLET")<<
  {
    Set dims = EvalSet(sXUnion, Set(Set reg)
    {
      Matrix x = reg[3];
      Real i   = Rows(x);
      Real j   = Columns(x); 
      SetOfReal(i,j)
    });
    Set coor = CoorTriplet(dims, 1, 1);
    For(1,Card(coor), Set(Real k)
    { coor[k]<<SetOfMatrix(sXUnion[k][3]) })
  };
  Set inputDef = (model1->InputDefinition) << (model2->InputDefinition);
  Set sigmaDef = (model1->SigmaDefinition) << (model2->SigmaDefinition);

  BLRModelDef(sY, sX, Empty, inputDef, sigmaDef)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Retorna la union de dos definiciones de modelos. Los conjuntos
 han de ser concatenables, es decir las matrices sparse definidas deben ser 
concatenables. Dos matrices sparse son concatenables el ultimo de sus bloques
 se puede concatenar con el primero de los otros bloques.", 
BLR_UnionModelDef);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix BLR_P(Set model, Real calls, Real burning, Text fileSample)
//////////////////////////////////////////////////////////////////////////////
{
  Set modelP = BLR_AddPrior(model);
  BLR(modelP, calls, burning, fileSample)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Regresión lineal a bayesiana con información a priori en los
parametros", BLR_P);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix BLR_H
(
  Set model, 
  Set hierSet, 
  Real calls, 
  Real burning, 
  Text fileSample
)
//////////////////////////////////////////////////////////////////////////////
{
  If(IsEmpty(hierSet), 
  BLR_P(model, calls, burning, fileSample),
  { 
    Set modelH = BLR_AddHierarchy(model, hierSet);
    BLR_P(modelH, calls, burning, fileSample)
  })  
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Regresión lineal a bayesiana con información a priori en los
parametros y jerarquia.", BLR_H);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix BLR_LinConBlock
(
  Set allParamNames, 
  Set outConNames, 
  Set inpConNames, 
  Matrix X
)
//////////////////////////////////////////////////////////////////////////////
{
  Real numAllPar  = Card(allParamNames);

  Set index       = Range(1, numAllPar, 1);
  Set indexOutCon = 
   Select(index, Real(Real i){allParamNames[i] <: outConNames});
  Set indexInpCon = 
   Select(index, Real(Real i){allParamNames[i] <: inpConNames});

  Matrix one_     = Rand(1,1,-1,-1);

  Matrix matrixOut = Group("ConcatRows", EvalSet(indexOutCon, Matrix(Real i)
  {
    FullWithZeroCol(one_, i, numAllPar)
  })); 

  Matrix matrixInp = SetSum( For(1, Card(inpConNames), Matrix(Real i)
  {
    FullWithZeroCol(SubCol(X, SetOfReal(i)), indexInpCon[i], numAllPar)
  }));

//Text WriteLn("matrixOut"<<matrixOut);
//Text WriteLn("matrixInp"<<matrixInp);
  matrixOut+matrixInp
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("",
BLR_LinConBlock);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set BLR_AddLinCon(Set model, Set linConModel)
//////////////////////////////////////////////////////////////////////////////
{
  Set sY          = model->SY;
  Set sX          = model->SX;
  Set inputDef    = model->InputDefinition;
  Set sigmaDef    = model->SigmaDefinition;

  Set sYN         = DeepCopy(sY);
  Set sXN         = DeepCopy(sX);
  Set inputDefN   = inputDef+SetConcat(Traspose(linConModel)[4]);
  Set sigmaDefN   = DeepCopy(sigmaDef);

  Set allParamNames  = Traspose(inputDefN)[1];

  Set infoRowSX   = sX[Card(sX)];  
  Real lastRowSX  = infoRowSX[1]+Rows(infoRowSX[3])-1;

  Real   k = 1;
  Real While(LE(k, Card(linConModel)),
  {
    Set linConDef     = linConModel[k];
    Set paramOut      = linConDef->ParamOut;
    Set paramInp      = linConDef->ParamInp;
    Set sigmaParam    = linConDef->SigmaParamDef;
    Set sigmaParamDef = If(Not(IsEmpty(sigmaParam)), sigmaParam,
    {
      Text nameSigma = "Sigma.ConLin."+IntText(k);
      BLRParamDef(nameSigma, 0.1, Copy(Empty), Copy(Empty))                
    });
    Matrix xCon       = linConDef->XCon;
  
    Real lastRowSX := lastRowSX + 1;
    Real row = Copy(lastRowSX);
   
    Matrix mat = BLR_LinConBlock(allParamNames, paramOut, paramInp, xCon);

    Set sYBlock = SetOfMatrix(Rand(Card(paramOut), 1, 0, 0));
    Set sXBlock = SetOfAnything(row, 1, mat);

    Set sYN := sYN<<sYBlock;
    Set sXN := sXN<<SetOfSet(sXBlock);
    Set sigmaDefN := sigmaDefN<<SetOfSet(sigmaParamDef);
    
    Real lastRowSX := lastRowSX+Rows(mat);
    Real k := k+1;    
    k 
  }); 
  
  BLRModelDef(sYN, sXN, Empty, inputDefN, sigmaDefN)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Anade una conexion lineal entre parametros, 
se puede especificar el tipo de transformacion sobre los parametros y la 
matriz de covarianzas del bloque.",
BLR_AddLinCon);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set BLR2BLRC(Set model, Set linSet)
//////////////////////////////////////////////////////////////////////////////
{
  Set inputDef = model->InputDefinition;
  Real nPar = Card(inputDef);

  Set limInfo = For(1, nPar, Set(Real k)
  {
    Set reg        = inputDef[k];
    Set limInfoSet = reg->LimInfo;

    Real limInf    = If(IsEmpty(limInfoSet), -TheMaxAbsValue, 
    {
      Real limInfAux = limInfoSet->LimInf;
      If(IsUnknown(limInfAux), -TheMaxAbsValue, limInfAux)
    });
    Real limSup    = If(IsEmpty(limInfoSet), TheMaxAbsValue, 
    {
      Real limSupAux = limInfoSet->LimSup;
      If(IsUnknown(limSupAux),  TheMaxAbsValue, limSupAux)
    });  

    Matrix B = FullWithZeroCol(SetCol(SetOfReal(1, -1)), k, nPar);
    Matrix b = SetCol(SetOfReal(limSup, -limInf));
    SetOfMatrix(B, b)
  })<<SetOfSet(linSet);
  Set limInfoSel    = Select(limInfo, Real(Set reg){Not(IsEmpty(reg))});
  Set traLimInfoSel = Traspose(limInfoSel);
  Matrix B = Group("ConcatRows", traLimInfoSel[1]);
  Matrix b = Group("ConcatRows", traLimInfoSel[2]);
  SetOfMatrix(B, b)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Retorna un conjunto reultado de trasladar la informacion de
limites que tiene el conjunto model de estructura BLRModelDef a una estructura
de inecuaciones que se suma a la dada por B*beta <= b. Esta funcion se usa
para utilizar la funcion BLRC en vez una BLR.",
BLR2BLRC);
//////////////////////////////////////////////////////////////////////////////


