//////////////////////////////////////////////////////////////////////////////
// MODULE: stdlib.math.stat.model.arima.aia 
// THEME:  Automatic Intervention Analysis
// FILE:   aia.build_model.tol
// BRIEF:  Builds a model to be used in AIA by estimation or postulation
//////////////////////////////////////////////////////////////////////////////

//VBR_PENDENT: Falta traducir comentarios al ingl�s


//////////////////////////////////////////////////////////////////////////////
  Set Aia.PreBuildModel(Serie output, 
                        Set   arimaInfo,
                        Real  preestimIter)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn dif0    = ARIMAGetDIF(arimaInfo);
  Polyn ar0     = ARIMAGetAR (arimaInfo);
  Polyn ma0     = ARIMAGetMA (arimaInfo);
  Real  p       = Card(Monomes(1-ar0));
  Real  q       = Card(Monomes(1-ma0));
  Ratio arima0  = (dif0*ar0)/(ma0);
  Serie res0    = DifEq(arima0,output)+0*output;
  Set preEst0   = Aia.PreBuildModelInfo(arima0,res0,Empty);
  Set If(!preestimIter,
  {
    preEst0
  },
  {
    Real oldMIter         = Copy(MaxIter);
    Real oldDoStatistics  = Copy(DoStatistics);
    Real oldDoDiagnostics = Copy(DoDiagnostics);
    Real MaxIter         := preestimIter;
    Real DoStatistics    := 0;
    Real DoDiagnostics   := 0;
    Set   aitr  = Traspose(arimaInfo);  
    Real  per   = SetMax(aitr[1]);
    Set   AR    = aitr[2];  
    Set   MA    = aitr[3];  
    Polyn DIF   = ARIMAGetDIF(arimaInfo);
    If(!(p+q), preEst0,
    {
      Set  mDef = @ModelDef(output,1,0,per,0,DIF,AR,MA,Empty,Empty);
      Real Show(0,"ALL");
      Set  mEst = Estimate(mDef);
      Real Show(1,"ALL");
      Real MaxIter       := oldMIter;
      Real DoStatistics  := oldDoStatistics;
      Real DoDiagnostics := oldDoDiagnostics;
      If(!Card(mEst), preEst0, 
      {
        Polyn  ar     = SetProd(mEst[2][7]);
        Polyn  ma     = SetProd(mEst[2][8]);
        Ration arima  = (ar*DIF)/ma;
        Serie  res    = mEst[3][1]+0*output;
        Aia.PreBuildModelInfo(arima,res,mEst)
      })
    })
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Construye un modelo ARIMA de estructura dada por <arimaInfo> "
" como paso previo para aplicar la funci�n AIA a una serie <output>. "
"Si <preestimIter> es 0 o <arimaInfo> no tiene par�metros (p=q=0) entonces "
"se utiliza el modelo <arimaInfo> tal cual. En otro caso se usa <arimaInfo> "
"como punto inicial de la funci�n Estimate durante un m�ximo de "
"<preestimIter> iteraciones.\n"
"Se supondr� que la serie <output> ha sido previamente transformada si as� "
"fuera necesario.",
Aia.PreBuildModel);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
  Set Aia.SelectModel(Text name,
                      Serie output, 
                      @Aia.OptionsInfo options)
//////////////////////////////////////////////////////////////////////////////
{
  Set eval = For(1,Card(options->arimaDef_), Set(Real arimaIndex)
  {
    Set arimaInfo = (options->arimaDef_)[arimaIndex];
    Set preEstMod_0 = Aia.PreBuildModel(output,arimaInfo,
                                        options->preestimIter_);
    Ratio arima   = preEstMod_0->arima_;
    Serie res_0   = preEstMod_0->residuals_;
    Set   modEst  = preEstMod_0->model_;
    Real  N       = CountS(res_0);
    Real  p       = Degree(ARIMAGetAR(arimaInfo));  
    Real  q       = Degree(ARIMAGetMA(arimaInfo));  
  //WriteLn("TRACE N="<<N);
    If(N<options->minimumOutputLength_,
    {
      Aia.SelectModelInfo(arimaIndex,
                          output,filter=0*output,noise=output,res_0,
                          modEst,Empty,arima,
                          N,0,p,q,?,Empty,?,?,?)
    },
    {
      Set   aia     = If(!(options->optMaxOrder_),
                         AIA(res_0,arima,options->outlierShapes_),
                         AIA(res_0,arima,options->outlierShapes_,
                             options->optMaxOrder_,
                             options->optMaxCrossNum_,
                             options->optMinNonZeroParamProb_,
                             options->optMaxEigenValueRelativeRange_));
      Set   aiaInf  = Aia.OutlierInfo.BuildFromSetInpDef(aia);
      Date  first   = First(output);
      Date  last    = Last (output);
      Real  n       = Card(aia);
      Serie filter  = 
      {
        Serie aux = If(!Card(aia),output*0,GroupInputs(aia));
        aux-AvrS(aux-output)
      }; 
      Serie noise   = output-filter;
      Serie res     = DifEq(arima,noise);
      Real  sig     = StDsS(res);
      Real  S2      = SumS(res^2); 
      Real  rLh     = Log(0.5)+LogGamma(N/2)-0.5*N*(Log(2*PI)+Log(S2/2));
    //WriteLn("TRACE n="<<n);
    //WriteLn("TRACE sig="<<sig);
    //WriteLn("TRACE rLh="<<rLh);
      If(!n,
      {
        Real  tbic = -2*rLh+n*Log(N);
        Aia.SelectModelInfo
        (
          arimaIndex,output,filter,noise,res,modEst,aiaInf,arima,
          N,n,p,q,sig,Empty,rLh,0,tbic
        )
      },
      {
      //WriteLn("TRACE sig="<<sig);
        Matrix Y = Tra(SerSetMat(SetOfSerie(DifEq(arima, output))));
        Matrix X = Tra(SerSetMat(EvalSet(aia, Serie(Set inp)
        {
        //WriteLn("inp->X="<<Identify(inp->X));
          DifEq(arima,SubSer(inp->X,first,last))
        })));
      //WriteLn("TRACE rows(X)="<<Rows   (X));
      //WriteLn("TRACE cols(X)="<<Columns(X));
      //WriteLn("TRACE Xt="<<Tra(X));
        Set    svd = SVD(X);
        Matrix L   = svd[2]*Tra(svd[3]);
      //WriteLn("TRACE L="<<L);
        Matrix b = svd[3]*InverseDiag(svd[2])*(Tra(svd[1])*Y);
      //WriteLn("TRACE b="<<b);
        Set    aiaInf_b = For(1, n, Set(Real k_)
        {
          @Aia.OutlierInfo
          (
            aiaInf[k_]->shape_,
            aiaInf[k_]->dating_,
            aiaInf[k_]->date_,
            MatDat(b,k_,1)
          ) 
        });
        Matrix t   = RProd(Tra(L)*b,1/sig);
      //WriteLn("TRACE t="<<t);
        Set    tlh = For(1,n,Real(Real k)
        {
          Log(1-2*DistT(-Abs(MatDat(t,k,1)),N-n))
        });
        Real  tLh  = SetSum(tlh);
        Real  lh   = rLh + tLh;
        Real  tbic = -2*lh+n*Log(N);
      //WriteLn("TRACE rLh="<<rLh+" tLh="<<tLh+" tbic="<<tbic);
        Aia.SelectModelInfo
        (
          arimaIndex,output,filter,noise,res,modEst,aiaInf_b,arima,
          N,n,p,q,sig,svd,rLh,tLh,tbic
        )
      })
    })
  });
  Set eval.valid = Select(eval,Real(Aia.SelectModelInfo a)
  {
    And(Card(a),Not(IsUnknown(a->tbic_)))
  });
  Set selected = If(!Card(eval.valid),Empty,
  {
    Set aux=Sort(eval.valid,Real(Aia.SelectModelInfo a, Aia.SelectModelInfo b)
    {
      Compare(a->tbic_,b->tbic_)
    });
    aux[1] 
  });
  Eval("Aia.SelectModel."+name+"=selected")
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Aplica la funci�n AIA a la lista de estruturas ARIMA dada "
"en options->arimaDef_ y selecciona el modelo que alcanza el menor criterio "
"de informaci�n bayesiano, el cual se ha modificado para que se incluya la "
"probabilidad de irreductibilidad de los inputs.\n"
"Se supondr� que la serie <output> ha sido previamente transformada si as� "
"fuera necesario.",
Aia.SelectModel);
//////////////////////////////////////////////////////////////////////////////



