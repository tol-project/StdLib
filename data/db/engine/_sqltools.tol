//////////////////////////////////////////////////////////////////////////////
// FILE    : _sqltools.tol
// PURPOSE : Funciones de utilidad de SQL
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// STRUCT
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
// CONSTANTS
//////////////////////////////////////////////////////////////////////////////
Set CalendarSet = SetOfText(
  "MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY","SUNDAY",
  "JANUARY","FEBRUARY","MARCH"    ,"APRIL"  ,"MAY"     ,"JUNE",
  "JULY"   ,"AUGUST"  ,"SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER",
  "DAY1" ,"DAY2" ,"DAY3" ,"DAY4" ,"DAY5" ,"DAY6" ,"DAY7" ,"DAY8" ,"DAY9",
  "DAY10","DAY11","DAY12","DAY13","DAY14","DAY15","DAY16","DAY17","DAY18",
  "DAY19","DAY20","DAY21","DAY22","DAY23","DAY24","DAY25","DAY26","DAY27",
  "DAY28","DAY29","DAY30","DAY31");

//////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Real SqlTableUpdateCodeDes(Text     table,  Text    field,
                           Anything valOld, Anything valNew,
                           Real     gestor)
//////////////////////////////////////////////////////////////////////////////
{
  // Obtenemos informacion de las tablas referenciadas
WriteLn("Obteniendo informaci�n de las tablas dependientes de "+table+" ...");
  Set setTabFk = SqlInfoDependentAll(table, gestor);
  // Deshabilitamos las referencias
  Set EvalSet(setTabFk, Real(Set set)
  {
WriteLn("Deshabilitando: "+ set->Table +"  -  "+ set->Index +" ...");
     Text sql = SqlForeignDisable(set->Table, set->Index, gestor);
     Real SqlDBExecQuery(sql)
  });
  // Actualizamos la tabla principal
WriteLn("Actualizando la tabla : "+ table +" ...");
  Text sqlPri =
  "update "+ table +" set "+
     field +" = "+ SqlFormatAny(valNew, gestor) +"
   where "+ field +" = "+ SqlFormatAny(valOld, gestor);
  Real okUpd = SqlDBExecQuery(sqlPri);
  // Modificamos los valores de las tablas refereidas
  Set If(okUpd,
  {
    Set EvalSet(setTabFk, Real(Set set)
    {
WriteLn("Actualizando la tabla : "+ set->Table +" ...");
      Text sql =
      "update "+ set->Table +" set "+
        set->Column +" = "+ SqlFormatAny(valNew, gestor) +"
       where "+ set->Column +" = "+ SqlFormatAny(valOld, gestor);
       Real SqlDBExecQuery(sql)
    })
  }, Empty);
  // Habilitamos las referencias
  Set EvalSet(setTabFk, Real(Set set)
  {
WriteLn("Habilitando: "+ set->Table +"  -  "+ set->Index +" ...");
     Text sql = SqlForeignEnable(set->Table, set->Index, gestor);
     Real SqlDBExecQuery(sql)
  });
  // return
  okUpd
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Actualizaci�n del campo codigo de una tabla. Se obtinenen todas las tablas
dependientes, se deshabilitan las calves ajenas, se modifican los valores y
se habilitan las claves ajenas.
Par�metros:
  table  -> Tabla a modificar
  field  -> Campo a modificar
  valOld -> Valor antiguo
  valNew -> Valor nuevo
  gestor -> SGBD activo 
Devuelve: 1 -> OK, 0 -> Error
",SqlTableUpdateCodeDes);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Text SqlPrsType2Tol(Text typTxt)
//////////////////////////////////////////////////////////////////////////////
{
  Text typLow = ToLower(Compact(typTxt));
  Text Case(
    typLow=="char",          "Text", // Caracteres
    typLow=="varchar",       "Text", // Caracteres longitud variable
    typLow=="varchar2",      "Text", // Caracteres longitud variable ORACLE
    typLow=="long",          "Text", // Caracteres
    typLow=="number",        "Real", // Numero
    typLow=="decimal",       "Real", // Numero
    typLow=="numeric",       "Real", // Numero equivalente a decimal.
    typLow=="float",         "Real", // Numero en coma flotante
    typLow=="double",        "Real", // Numero en coma flotante
    typLow=="real",          "Real", // Numero en coma flotante
    typLow=="bit",           "Real", // Numero entero con valor 1 � 0
    typLow=="integer",       "Real", // Numero entero
    typLow=="money",         "Real", // Numero en coma flotante SQLServer
    typLow=="smallmoney",    "Real", // Numero en coma flotante SQLServer
    typLow=="bigint",        "Real", // Numero entero SQLServer
    typLow=="int",           "Real", // Numero entero (abreviatura)
    typLow=="smallint",      "Real", // Numero entero SQLServer
    typLow=="tinyint",       "Real", // Numero entero SQLServer
    typLow=="date",          "Date", // Fecha Oracle
    typLow=="datetime",      "Date", // Fecha SQLServer
    typLow=="smalldatetime", "Date", // Fecha SQLServer
    1,                       "Unknown"
  )
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Retorna el tipo TOL equivalente a un tipo SQL.",
SqlPrsType2Tol);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real SqlTableUpdateFields
(
  Text idTable,
  Set  datWhe,
  Set  datUp
)
//////////////////////////////////////////////////////////////////////////////
{
    Set exprFilDat = EvalSet(datUp, Text(Set datUp)
    { datUp[1] +" = "+ SqlFormatAny(datUp[2], GesAct) }); 

    Text dataUpd = TxtListItem(exprFilDat," , " );
    Set exprFilWhe = EvalSet(datWhe, Text(Set datWhe)
    { "( "+ datWhe[1] +" = "+ SqlFormatAny(datWhe[2], GesAct) +" )" }); 

    Text dataWhe = TxtListItem(exprFilWhe," and " );

    Text query =
    "update "+ idTable +"
     set    "+ dataUpd +"
     where  "+ dataWhe;
    
    Real SqlDBExecQuery(query)    
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Actualiza la tabla <idTable> donde el conjunto <datUp> corresponde a los 
campos no primarios a actualizar y el conjunto <datWhe> que corresponde a los 
campos de la clave primaria a filtrar.
datUp  -> SetOfSet (SetOfAnything(campo, valor))
datWhe -> SetOfSet (SetOfAnything(campo, valor))",
SqlTableUpdateFields);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real SqlTableUpdateFields2
(
  Text idTable,
  Set  datWhe,
  Set  datUp
)
//////////////////////////////////////////////////////////////////////////////
{
    Set exprFilDat = EvalSet(datUp, Text(Set datUp)
    { datUp[1] +" = "+ datUp[2] }); 

    Text dataUpd = TxtListItem(exprFilDat," , " );

//WriteLn("dataUpd: "<<dataUpd);
    Set exprFilWhe = EvalSet(datWhe, Text(Set datWhe)
    { "( "+ datWhe[1] +" = "+ SqlFormatAny(datWhe[2], GesAct) +" )" }); 

    Text dataWhe = TxtListItem(exprFilWhe," and " );

    Text query =
    "update "+ idTable +"
     set    "+ dataUpd +"
     where  "+ dataWhe;
//WriteLn("query: "<<query);
//Real 0
    Real SqlDBExecQuery(query)    
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Actualiza la tabla <idTable> donde el conjunto <datUp> corresponde a los 
campos no primarios a actualizar y el conjunto <datWhe> que corresponde a los 
campos de la clave primaria a filtrar.
Modificaci�n para no utilizar delimitadores de campos.
datUp  -> SetOfSet (SetOfAnything(campo, valor))
datWhe -> SetOfSet (SetOfAnything(campo, valor))",
SqlTableUpdateFields);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real SqlTableInsertFields
(
  Text idTable,
  Set  datUp
)
//////////////////////////////////////////////////////////////////////////////
{
    Set exprFields = EvalSet(datUp, Text(Set datUp)
    { datUp[1]}); 

    Set exprValues = EvalSet(datUp, Text(Set datUp)
    { SqlFormatAny(datUp[2], GesAct)}); 

    Text textFields = TxtListItem(exprFields,",  " );

    Text textValues = TxtListItem(exprValues,",  " );

    Text query =
    "insert into "+ idTable +"
     ( "+ textFields +" )
     values
     ( "+ textValues +" )"; 

    Real SqlDBExecQuery(query)    
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Inserta en la tabla <idTable> donde el conjunto <datUp> corresponde a los 
campos que se quieren insertar con su valor.
datUp  -> SetOfSet (SetOfAnything(campo1, valor1), 
                    SetOfAnything(campo2, valor2))",
SqlTableInsertFields);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real SqlTableUpdateCodeRep(Text     table,  Text    field,
                           Anything valOld, Anything valNew,
                           Real     gestor)
//////////////////////////////////////////////////////////////////////////////
{
  Real If( Not(valOld==valNew),
  {
WriteLn("Obteniendo informaci�n de las tablas dependientes de "+table+" ...");
    Set setTabFk = SqlInfoDependentAll(table, gestor);
    // Paso 1: Insertamos el valor nuevo en la tabla de origen
    // obtenemos las columnas de la tabla para montar la insercion
WriteLn("Insertando "+valNew+" en "+table+".");
    Set columns = SqlInfoColumns(table, gestor);
    Set fields = EvalSet(columns,Text(Set item)
    {
      item[2]
    });
    Text txtFields = "("<< TxtListItem(fields, ", ") <<")";
    // obtenemos el valor del registro (tupla) antiguo de la base de datos
    Text queryValues = 
      "select * from "+table+" where "+field+" = '"+valOld+"'";
    Set setValues = SqlDBTable (queryValues,"");
    // al ser clave, solo devolvera una tupla
    Set mySetValues = setValues[1];
    // reemplazamos solo el valor de la clave a cambiar
    Set setValuesReplaced = For(1, Card(setValues[1]), Anything(Real numberValue) 
    {
      // si la clave, se reemplaza
      Text dataType = columns[numberValue][3];
      Anything If( fields[numberValue]==Text(field), 
      { // se reemplaza      
        valNew
      },
      { // se conserva el valor
        mySetValues[numberValue]
      })
    });
    // aplicamos los separadores dependiendo del tipo de campo
    Text txtValuesReplaced = Text TxtFormList(Set  setValuesReplaced,
                  "(", // Inicio de la lista
                  ")", // Final  de la lista
                  ", ", // Separador de la lista
                 "'", // Delimitacion de textos
                 "", // Formato para numeros reales
                 "", // Delimitacion de fechas
                 ""); // Formato para fechas
    // se monta el insert y se ejecuta
    Text txtQueryInsert = "insert into "+table+" values "+txtValuesReplaced;
WriteLn("txtQueryInsert: "<<txtQueryInsert);
    Real okIns1 = SqlDBExecQuery(txtQueryInsert);
    // Si no ha habido error en el paso 1, continuamos
    Real If (okIns1,
    {
      // Paso 2: Insertamos en todas las tablas relacionadas
      Set EvalSet(setTabFk,Text(Set itemTable)
      {    
        // se obtiene el nombre de la tabla cuya Fk se�ala a la tabla que
        // se desea cambiar
        Text nameItemTable = itemTable[1];
        //si la ForeignColumn es la que hemos cambiado
        Real If(And(itemTable[6]==field, itemTable[4]==table),
        {
          // nombre del campo en la tabla que referencia
          Text fieldNew = itemTable[3];
          // nombre de la tabla que referecia
          Text tableNew = itemTable[1];
          //insertar una linea con el nuevo codigo
WriteLn("Insertando "+valNew+" en el campo "+fieldNew+" de "+tableNew+".");
          // adquiero los valores antiguos de la tabla
          Text queryValuesNew = 
            "select * from "+tableNew+" where "+fieldNew+" = '"+valOld+"'";
          Set setValuesNew = SqlDBTable (queryValuesNew,"");
          // se recorren los registros devueltos que se deben cambiar
          Set If(GT(Card(setValuesNew),0),
          { // ha devuelto elementos, se van a�adiendo tantas columnas como
            // elementos haya uno a uno:
            // en primer lugar tomamos los nombres de las columnas
            Set columnsNew = SqlInfoColumns(tableNew, gestor);
            // nombres de los campos
            Set fieldsNew = EvalSet(columnsNew,Text(Set item)
            {
              item[2]
            });
            // se preparan para el insert
            Text txtFieldsNew = "("<< TxtListItem(fieldsNew, ", ") <<")"; 
            // obtenemos los valores para cada campo y se reemplaza el 
            // que se haya seleccionado
            Set EvalSet(setValuesNew, Set(Set item)
            {
              // se cambia cada uno de los registros devueltos       
              Set mySetValuesNew = setValuesNew[1];
              // se reemplaza solo el valor del campo
              Set setValuesReplacedNew = For(1, Card(setValuesNew[1]), 
                                             Anything(Real numberValue) 
              {
                Text dataType = columnsNew[numberValue][3];
                Anything If( fieldsNew[numberValue]==Text(fieldNew), 
                { // se reemplaza      
                  valNew
                },
                { // se conserva el valor
                  mySetValuesNew[numberValue]
                })
              }); 
              // se forma la lista de valores para el insert
              Text txtValuesReplacedNew = Text TxtFormList(
                                            Set  setValuesReplacedNew,
                    "(", // Inicio de la lista
                    ")", // Final  de la lista
                    ", ", // Separador de la lista
                    "'", // Delimitacion de textos
                    "", // Formato para numeros reales
                    "'", // Delimitacion de fechas
                    ""); // Formato para fechas
               // ejecutamos el insert del nuevo valor
               Text txtQueryInsertNew = 
                      "insert into "+tableNew+" values "+txtValuesReplacedNew;
               Real okIns2Tmp = SqlDBExecQuery(txtQueryInsert);
              Copy(Empty)       
            }) // Fin Set EvalSet
         },
         { // no ha devuelto elementos
           WriteLn("No se han encontrado elementos que reemplazar.");
           Copy(Empty)
         });
         1
       },
       { // no nos afecta
         0
        });
       ""
      });
    //fin de inserci�n en todas las tablas relacionadas
  
    // Paso 3: Comienzo del borrado de todas las claves antiguas en las 
    //         tablas que la usan como clave ajena
    Set setDependentTables2 = For(1, Card(setTabFk), Real(Real indexItem)
    {    
      Set itemTable = setTabFk[Card(setTabFk)-indexItem+1];
      Text nameItemTable = itemTable[1];
      // nombre del campo en la tabla que referencia
      Text fieldNew = itemTable[3];
      // nombre de la tabla que referecia
      Text tableNew = itemTable[1];
WriteLn("Borrando "+valOld+" en el campo "+fieldNew+" de "+tableNew+".");
      Text txtQueryDelete = 
             ("delete from "+tableNew+" where ("+fieldNew+" =  '"+valOld+"')");

      Real SqlDBExecQuery(txtQueryDelete);   
      0
    }); //fin del borrrado en  tablas relacionadas

    // Paso 4: Borrado de la clave antigua en la propia tabla
WriteLn("Borrando "+valOld+" en el campo "+field+" de "+table+".");
     Text txtQueryDelete = 
             ("delete from "+table+" where ("+field+" =  '"+valOld+"')");
     Real SqlDBExecQuery(txtQueryDelete)
    },
    {
      okIns1
    }) // fin del borrado en la propia tabla
  },
  { // Error: se intenta reemplazar un valor por el mismo
    0
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Actualizaci�n del campo codigo de una tabla. Se inserta un registro en la
tabla origen con el nuevo valor de la clave. Se insertan registros en las
tablas que dependan de esta con la nueva clave. Por ultimo, se borran los 
registros con la clave antigua en estas tablas y en la de origen.
Par�metros:
  table  -> Tabla a modificar
  field  -> Campo a modificar
  valOld -> Valor antiguo
  valNew -> Valor nuevo
  gestor -> SGBD activo 
Devuelve: 1 -> OK, 0 -> Error
",SqlTableUpdateCodeRep);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text SqlGetWhereCalendar(Set calendar, Text field, Real gestor)
//                                                                 
//////////////////////////////////////////////////////////////////////////////
{
  // format DW
  Text fmtDW = SqlGetDatePart("dw", gestor);
  // eval set
  Set setWheDW = EvalSet(calendar, Text (Text cln) {
    Text calen = ToUpper(cln);
    // Day of the week
    Text nDW = Case
    (
      calen == "MONDAY",     "1", 
      calen == "TUESDAY",    "2",
      calen == "WEDNESDAY",  "3",
      calen == "THURSDAY",   "4",
      calen == "FRIDAY",     "5",
      calen == "SATURDAY",   "6",
      calen == "SUNDAY",     "7",
      True, ""
    );
    If(nDW=="", "", SqlFieldToStr(field, fmtDW, gestor) +" = '"+ nDW +"' ")
  });
  // format Month
  Text fmtMonth = SqlGetDatePart("m", gestor);
  // eval set
  Set setWheMon = EvalSet(calendar, Text (Text cln) {
    Text calen = ToUpper(cln);
    // Month
    Text nMonth = Case
    (
      calen == "JANUARY",   "1",
      calen == "FEBRUARY",  "2",
      calen == "MARCH",     "3",
      calen == "APRIL",     "4",
      calen == "MAY",       "5",
      calen == "JUNE",      "6",
      calen == "JULY",      "7",
      calen == "AUGUST",    "8",
      calen == "SEPTEMBER", "9",
      calen == "OCTOBER",  "10",
      calen == "NOVEMBER", "11",
      calen == "DECEMBER", "12",
      True, ""
    );
    If(nMonth=="", "", 
      SqlFieldToStr(field, fmtMonth, gestor) +" = "+ nMonth +" ")
  });
  // format Day Month
  Text fmtDayMon = SqlGetDatePart("d", gestor);
  // eval set
  Set setWheDayMon = EvalSet(calendar, Text (Text cln) {
    Text calen = ToUpper(cln);
    // Day of Month
    Text nDayMon = Case
    (
      calen == "DAY1",   "1",
      calen == "DAY2",   "2",
      calen == "DAY3",   "3",
      calen == "DAY4",   "4",
      calen == "DAY5",   "5",
      calen == "DAY6",   "6",
      calen == "DAY7",   "7",
      calen == "DAY8",   "8",
      calen == "DAY9",   "9",
      calen == "DAY10", "10",
      calen == "DAY11", "11",
      calen == "DAY12", "12",
      calen == "DAY13", "13",
      calen == "DAY14", "14",
      calen == "DAY15", "15",
      calen == "DAY16", "16",
      calen == "DAY17", "17",
      calen == "DAY18", "18",
      calen == "DAY19", "19",
      calen == "DAY20", "20",
      calen == "DAY21", "21",
      calen == "DAY22", "22",
      calen == "DAY23", "23",
      calen == "DAY24", "24",
      calen == "DAY25", "25",
      calen == "DAY26", "26",
      calen == "DAY27", "27",
      calen == "DAY28", "28",
      calen == "DAY29", "29",
      calen == "DAY30", "30",
      calen == "DAY31", "31",
      True, ""
    );
    If(nDayMon=="", "", 
      SqlFieldToStr(field, fmtDayMon, gestor) +" = "+ nDayMon +" ")
  });
  // build filter
  Set setWheDW2 = Select(setWheDW, Real(Text t) {
    t!=""
  });
  Set setWheMon2 = Select(setWheMon, Real(Text t) {
    t!=""
  });
  Set setWheDayMon2 = Select(setWheDayMon, Real(Text t) {
    t!=""
  });

  Text wheDW     = TxtListItem(setWheDW2,     " or ");
  Text wheMon    = TxtListItem(setWheMon2,    " or ");
  Text wheDayMon = TxtListItem(setWheDayMon2, " or ");
  // 
  // union filter
  Set setFil = SetOfText(wheDW, wheMon, wheDayMon);
  Set setFil2 = Select(setFil, Real(Text t) {
    t!=""
  });

  Text If(IsEmpty(setFil2), "", TxtFormListDel(setFil2, 
      "(", ")", " and ", "(", ")", "", "", "", "", "", ""))
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Obtenemos el filtro SQL a ejecutar para un conjunto de valores del calendario
Para los valores que pertenecen al mismo dominio (por ejemplo los meses del
a�o) se usa el operador l�gico 'or'. Para unir los distintos dominios se usa
el operador l�gico 'and'.
Par�metros:
  calendar: Conjunto de textos con los valores del calendario
  field:    Nombre del campo en la consulta SQL
  gestor:   SGBD activo 
Devuelve: La sentencia SQL a usar
",SqlGetWhereCalendar);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text SqlGetWhereCalendarTms(Set calendar, Text field, Date iniDate, 
  Date endDate, Real gestor)
//                                                                 
//////////////////////////////////////////////////////////////////////////////
{
  Date dtIni = If(IsUnknownDate(iniDate), DefFirst, iniDate);
  Date dtEnd = If(IsUnknownDate(endDate), DefLast,  endDate);
  // eval set calendar
  Set setDtFmt = EvalSet(calendar, Set(Text tms) {
    Set setDates = Dates(Eval(tms), dtIni, dtEnd);
    // eval set dates
    Set EvalSet(setDates, Text(Date dt) {
      Text SqlFormatDate(dt, GesAct)
    })
  });
  Set setUqDtFmt = SetConcat(setDtFmt);
  Set setUqDtFmt2 = Select(setUqDtFmt, Real(Text t) {
    t!=""
  });    
  Text If(IsEmpty(setUqDtFmt2), "", TxtFormListDel(setUqDtFmt2, 
    " ( ", " ) ", " or ", field+" = ", "", "", "", "", "", "", ""))
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Obtenemos el filtro SQL a ejecutar para un conjunto de TimeSet. Para cada uno
de ellos obtenemos las fechas existentes, y sobre estas construimos la
sentencia SQL.
Par�metros:
  calendar: Conjunto de TimeSet pasados como textos
  field:    Nombre del campo en la consulta SQL
  dateIni:  Fecha desde la que calcular las fechas
  dateIni:  Fecha hasta la que calcular las fechas
  gestor:   SGBD activo 
Devuelve: La sentencia SQL a usar
",SqlGetWhereCalendarTms);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text SqlReplaceTable(Text txt, Set tabRep, Real gestor)
//////////////////////////////////////////////////////////////////////////////
{
  Text Case
  (
    EQ(gestor, GesTer), {
       Text WriteLn("Funcion por implementar"); "" },
    1, SqlReplaceTableMic(txt, tabRep)
  )
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"Construye la expresion SQL de Replace a partir de un texto con el nombre del
campo a reemplazar y una tabla de reemplazamientos igual que la funcion TOL
ReplaceTable.
Set SqlReplaceTable.Sample.Tab = SetOfSet(
[[ \"_\", \"\" ]],
[[ \"*\", \"\" ]],
[[ \".\", \"\" ]],
[[ \"#\", \"\" ]]
);
Text SqlReplaceTable.Sample.Txt = SqlReplaceTable(\"DB_FIELD\",
                                                  SqlReplaceTable.Sample.Tab,
                                                  GesAct);
",
SqlReplaceTable);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text SqlReplaceTableMic(Text txt, Set tabRep)
//////////////////////////////////////////////////////////////////////////////
{
  If(IsEmpty(tabRep), txt,
  {
    Set fstRep = tabRep[1];
    Text txtRep = "replace("+ txt +", '"+ fstRep[1] +"', '"+ fstRep[2] +"')";
    Set resRep = If(EQ(Card(tabRep), 1),
                    Empty,
                    ExtractByIndex(tabRep, Range(2, Card(tabRep), 1)));               
    SqlReplaceTableMic(txtRep, resRep)
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription(
"
SqlReplaceTable para el gestor Mic.
Ver descripcion de SqlReplaceTable.
",
SqlReplaceTableMic);
//////////////////////////////////////////////////////////////////////////////

