//////////////////////////////////////////////////////////////////////////////
// FILE    : _progressbar.tol
// PURPOSE : Provides a set of functions to display in the tcl/tk interface
//       a progressbar, useful to be displayed when computing large processes
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Text TclProgressBarCreate(Set valArg)
//  title,    // title for the progress bar window
//  width,    // width for the progress bar window in characters
//  height,   // height for the progress bar window in characters
//  breakvar // tol variable which when set to 1 progressbar dialogs ends
//////////////////////////////////////////////////////////////////////////////
{
  Set defArg = SetOfSet (
    @TclArgSt("-title", ""),
    @TclArgSt("-width", "20"),
    @TclArgSt("-height", "2"),
    @TclArgSt("-breakvar", "")
  );
  Set aplArg = TclMixArg( TclMixArg(SeedArg,defArg), valArg );

  Text TclEvalT("::progressbar::Show {" + TclGAV("-title", aplArg) + "} " +
    TclGAV("-width", aplArg) + " " + TclGAV("-height", aplArg) + " " +
    TclGAV("-breakvar", aplArg)
  )
};
PutDescription(
"
Funci�n que muestra una ventana con un cuadro de di�logo que tiene un mensaje
y una barra de progreso.
Mientras la barra de progreso est� mostrada no puede hacerse nada m�s desde el
interfaz, esto no afecta al codigo tol.
Podemos establecer el titulo de la ventana, el ancho
y el alto en caracteres, y opcionalmente una variable tol. Si esta variable
tiene valor el cuadro de dialogo se crear� con un boton abortar, que provocar�
que se destruya el di�logo y se establezca a 1 la variable tol con dicho
nombre.
",TclProgressBarCreate);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TclProgressBarSetText(Text text)
//////////////////////////////////////////////////////////////////////////////
{
  Text TclEvalT("::progressbar::SetText {" + text + "}")
};
PutDescription(
"
Establece en cualquier momento el texto mostrado en el di�logo de la barra de
progreso.
",TclProgressBarSetText);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TclProgressBarSet(Text percent)
//////////////////////////////////////////////////////////////////////////////
{
  Text TclEvalT("::progressbar::Set "+ percent)
};
PutDescription(
"
Establece el porcentaje de la barra de progreso, debe estar comprendido entre
0 y 100.
",TclProgressBarSet);
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Text TclProgressBarDestroy(Real void)
//////////////////////////////////////////////////////////////////////////////
{
  Text TclEvalT("::progressbar::Destroy")
};
PutDescription(
"
Destruye la barra de progreso, dejando libre de nuevo el interfaz.
",TclProgressBarDestroy);
//////////////////////////////////////////////////////////////////////////////
