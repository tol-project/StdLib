//////////////////////////////////////////////////////////////////////////////
// FILE   : algMetSqlDep.tol
// PURPOSE: Metodo SQL.01 de obtenci�n de series a partir de la definici�n del
//          �lgebra. Se basa en extracci�n de datos desde una BBDD con
//          DBSeries mediante la construcci�n de una query de la forma:
// 
//               select <exp_date> as idDateValue,
//                      <exp_value> as value
//               from <exp_from>
//               [where] { <exp_where> }
//               group by <exp_date> (depending on -doGrBy)
//               order by idDateValue
// 
//           Cada una de las expresiones <exp_*> son generadas en funci�n de
//          los valores que toman ciertas dimensiones y subdimensiones, que 
//          se especifican en un conjunto *Dep y tambi�n se especifica la 
//          funci�n que forma la expresi�n *Fun.
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
// STRUCTURES and Pseudo-structures
//////////////////////////////////////////////////////////////////////////////


Struct @AlgMetSqlDepExtSt
{
  Code DatFun, // Funci�n que construye exp_date
  Code ValFun, // Funci�n que construye exp_value
  Code FroFun, // Funci�n que construye exp_from
  Code WheFun, // Funci�n que construye exp_where
  Code FecFun,  // Funci�n que devuelve el fechado de la serie
  Code SetParFun // Funci�n que devuelve un conjunto de parametros con
                 // estructura @TclArgSt, para por ejemplo:
                 // -defVal: determinar el valor por defecto de DBSeries
                 // -traQry: Para trazar o no la query
                 // -traTms: Para trazar el tiempo que tarda la query
                 // -doGrBy: Para realizar el group by en la query 
};

Set AlgMetSqlDepSt(
  Code DatFun, // Funci�n que construye exp_date
  Code ValFun, // Funci�n que construye exp_value
  Code FroFun, // Funci�n que construye exp_from
  Code WheFun, // Funci�n que construye exp_where
  Code FecFun)
{
  Set @AlgMetSqlDepExtSt(DatFun, ValFun, FroFun, WheFun, FecFun, FunEmpty)
};

Struct @AlgMetSqlFroSt
{
  Text Table,
  Text Alias
};

Set defParamsForMetSqlDep = SetOfSet(
    @TclArgSt("-defVal", "0"),
    @TclArgSt("-traQry", "1"),
    @TclArgSt("-traTms", "1"),
    @TclArgSt("-doGrBy", "1")
);

//////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
Text MetSqlDepGetQuery (
  Set  algSetNode,  // 
  Set  metSqlDep,    // Conjunto con estructura AlgMetSqlDepExtSt
  Text serName)
//////////////////////////////////////////////////////////////////////////////
{

  Code datFun = metSqlDep->DatFun;
  Text datExp = datFun(algSetNode);
  Code valFun = metSqlDep->ValFun;
  Text valExp = valFun(algSetNode);
  Code froFun = metSqlDep->FroFun;
  Text froExp = froFun(algSetNode);
  Code wheFun = metSqlDep->WheFun;
  Text wheExp = wheFun(algSetNode);
  Code parFun = metSqlDep->SetParFun;
  Set  parVal = parFun(algSetNode);

  Set params = TclMixArg(defParamsForMetSqlDep, parVal);
  Real doGrBy = Eval(TclGAV("-doGrBy", params));

  Text query =
    "select "+datExp+" as idDateValue,"   +NL+
            TextIndent(valExp,7)+" as val"+NL+
    "from "+ froExp                       +NL+
    If(wheExp!="","where "+wheExp+NL,wheExp)+
    If(doGrBy, "group by "+datExp+NL, "")+
    "order by idDateValue";
  query
};


//////////////////////////////////////////////////////////////////////////////
Serie MetSqlDepGetSerie (
  Set  algSetNode,  // 
  Set  metSqlDep,    // Conjunto con estructura AlgMetSqlDepExtSt
  Text serName)
//////////////////////////////////////////////////////////////////////////////
{
  Code fecFun = metSqlDep->FecFun;
  Text fechado = fecFun(algSetNode);
  Code parFun = metSqlDep->SetParFun;
  Set  parVal = parFun(algSetNode);

  Set params = TclMixArg(defParamsForMetSqlDep, parVal);
  Real defVal = Eval(TclGAV("-defVal", params));
  Real traQry = Eval(TclGAV("-traQry", params));
  Real traTms = Eval(TclGAV("-traTms", params));

  Text query = MetSqlDepGetQuery(algSetNode,metSqlDep,serName);
  Text If(traQry, WriteLn(TextIndent(query,6)), "");

  Set res = DBSeries(
        query,
        Eval("TimeSet "+fechado+";"),
        SetOfText(serName+"_"),
        SetOfText(serName+"_"),
        defVal);

  Serie If(IsEmpty(res), 
  {
    Real Msg("MetSqlDepGetSerie", "No hay serie");
    Serie CalInd(W, Eval("TimeSet "+fechado+";") ) 
  },
  res[1]
  )
};

